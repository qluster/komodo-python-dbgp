FROM alpine:edge

RUN apk add --update python2 py2-pip \ 
  && pip2 install komodo-python-dbgp

EXPOSE 9000 9001

CMD ["pydbgpproxy", "-d 0.0.0.0:9000", "-i 0.0.0.0:9001"]
